<?php

namespace App\controller\controller_class;

class blog {

    public $id = "";
    public $category = "";
    public $title = "";
    public $body = "";
    public $image = "";
    public $author = "";
    public $tags = "";
    public $date = "";
    public $like = "";
    public $comment = "";

    public function __construct() {
        $server_connection = @mysql_connect("localhost", "root", "") or die("Server not connected");
        $datanbase_connect = mysql_select_db("personalblog") or die("Database not selected");
    }

    public function index() {
        $_blog_post = array();

        $query = mysql_query("SELECT * FROM blog LEFT JOIN tbl_category ON blog.category = tbl_category.id ORDER BY blog.id DESC") or die("Error...!!");
        while ($rows = mysql_fetch_assoc($query)) {
            $_blog_post[] = $rows;
        }

        return $_blog_post;
    }

    public function select($start, $form) {
        $_blog_post = array();

        $query = mysql_query("SELECT * FROM `blog` ORDER BY id DESC LIMIT $start, $form") or die("Blog Select Error At select()...!!");
        while ($rows = mysql_fetch_assoc($query)) {
            $_blog_post[] = $rows;
        }

        return $_blog_post;
    }

    public function select_for_sidebar() {
        $_blog_post = array();

        $query = mysql_query("SELECT * FROM `blog` ORDER BY id DESC LIMIT 6") or die("Error at select_for_sidebar()...!!");
        while ($rows = mysql_fetch_assoc($query)) {
            $_blog_post[] = $rows;
        }

        return $_blog_post;
    }

    public function store($category, $title, $body, $image, $author, $tags) {
        $result = mysql_query("INSERT INTO `blog`(`id`, `category`, `title`, `body`, `image`, `author`, `tags`) VALUES (NULL,'$category', '$title','$body','$image','$author','$tags')") or die("Database Error, Failed to store!!!!");
    }

    //This function also used for editing posts
    public function singlePost($id) {
        $query = mysql_query("SELECT * FROM `blog` WHERE `id` = '$id'") or die("Error at singlePost()...!!");
        $rows = mysql_fetch_assoc($query);
        return $rows;
    }

    public function update($id, $title, $body, $author, $tags) {
        $qurey = mysql_query("UPDATE `blog` SET `id` = '$id', `title` = '$title', `body` = '$body', `author` = '$author', `tags` = '$tags' WHERE `id`='$id'") or die("Error occured to update");
        header("loccation: all_posts.php");
    }

    public function delete($id) {
        $query = mysql_query("DELETE FROM `blog` WHERE `id` = '$id'") or die("Cannot DELETE..ERROR!!!");
    }

    public function pagination() {
        $query = mysql_query("SELECT * FROM `blog`") or die("Error at pagination() ...!!");
        $total_rows = mysql_num_rows($query);
        return $total_rows;
    }

    public function paginationByCategory($id) {
        $query = mysql_query("SELECT * FROM `blog` WHERE `id` = '$id'") or die("Error at paginationByCategory()...!!");
        $total_rows = mysql_num_rows($query);
        return $total_rows;
    }

    public function category() {
        $_all_catagory = array();
        $query = mysql_query("SELECT * FROM `tbl_category`") or die("Error to connent table");
        while ($rows = mysql_fetch_assoc($query)) {
            $_all_catagory[] = $rows;
        }
        return $_all_catagory;
    }

    public function insert() {
        
    }

    public function like($user_id, $post_id) {
        $query = mysql_query("SELECT * FROM likes WHERE user_id = '$user_id' AND post_id = '$post_id'");
        if (mysql_num_rows($query) == 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function commentInsert($post, $user_id, $user_name) {
        $post_id = $post['id'];
        $comment = $post['comment'];
        //print_r(array($post_id, $comment));
        //exit();
        $query = mysql_query("INSERT INTO `comment` (`id`, `comment`, `user_id`, `post_id`, `user_name`) VALUES (NULL, '$comment', '$user_id', '$post_id', '$user_name')") or die("Comments error");
    }

    public function commentShow() {
        $_comment = array();
        $query = mysql_query("SELECT * FROM `comment`");
        while ($rows = mysql_fetch_assoc($query)) {
            $_comment[] = $rows;
        }
        return $_comment;
    }

    public function related_post($category_id) {
        $_all_catagory = array();
        $query = mysql_query("SELECT * FROM `blog` WHERE `category` = '$category_id' ORDER BY `date` DESC") or die("Error at related_post()!!!");
        while ($rows = mysql_fetch_assoc($query)) {
            $_all_catagory[] = $rows;
        }
        return $_all_catagory;
    }

}
