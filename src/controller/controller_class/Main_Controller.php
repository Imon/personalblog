<?php

namespace App\controller\controller_class;

use App\controller\controller_class\DB;
use App\controller\controller_class\Format;

/*
 * Main_Controller class
 */

/**
 * 
 */
class Main_Controller {

    public $db; // object for Database class
    public $fm; // object for format class
    public $msg = array();
    public $error;

    function __construct() {
        $this->db = new DB();
        $this->fm = new Format();
    }

    public function insert_data_by_array($table, $data, $exclude = array()) {
        $fields = $values = array();
        if (!is_array($exclude))
            $exclude = array($exclude);
        foreach (array_keys($data) as $key) {
            if (!in_array($key, $exclude)) {
                $fields[] = $key;
                $values[] = "'" . mysqli_real_escape_string($this->db->link, $data[$key]) . "'";
            }
        }
        $fields = implode(",", $fields);
        $values = implode(",", $values);
        $query = "INSERT INTO $table ($fields) VALUES ($values)";
        return $this->db->insert($query);
    }

}

?>