<?php

namespace App\controller\controller_class;

use App\controller\controller_class\DB;

class Circular extends Main_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function store($files, $post) {
        if (!empty($files['file']['name'])) {
            for ($i = 0; $i < count($files['file']['name']); $i++) {
                $validextensions_for_image = array("jpeg", "jpg", "png");
                $ext = explode('.', basename($files['file']['name'][$i]));
                $file_extension = end($ext);
                if (!empty($files['file']['name']) && in_array($file_extension, $validextensions_for_image)) {
                    $targetPath = "../images/" . time() . '_' . $files['file']['name'][$i];
                    if (move_uploaded_file($files['file']['tmp_name'][$i], $targetPath)) {
                        $imageUrl[] = $targetPath;
                    }
                }
            }

            $data['title'] = $this->fm->sanitize($post['title']);
            $data['office_name'] = $this->fm->sanitize($post['office_name']);
            $data['source'] = $this->fm->sanitize($post['source']);
            $data['official_web'] = $this->fm->sanitize($post['official_web']);
            $data['job_category'] = $this->fm->sanitize($post['job_category']);
            $data['total_post'] = $this->fm->sanitize($post['total_post']);
            $data['app_start_date'] = $this->fm->sanitize($post['app_start_date']);
            $data['app_end_date'] = $this->fm->sanitize($post['app_end_date']);
            $data['salary'] = $this->fm->sanitize($post['salary']);
            $data['application_link'] = $this->fm->sanitize($post['application_link']);
            $data['job_location'] = $this->fm->sanitize($post['job_location']);

            if (!empty($imageUrl)) {
                $multiple_img = implode(', ', $imageUrl);
                $data['image'] = $multiple_img;
            }
            $query = $this->insert_data_by_array('job_circular', $data);

            if ($query) {
                echo "<script>alert('New record created successfully');</script>";
            } else {
                echo "Error: " . $query . "<br>" . mysqli_error($this->connection);
            }
        } else {
            echo "<script>alert('At least one image required');</script>";
        }
    }

    public function select() {
        $query = "SELECT * FROM job_circular";
        $result = $this->db->select($query);
        return $result;
    }

    public function select_home($start_from, $post_per_page) {
        $query = "SELECT * FROM job_circular ORDER BY id DESC LIMIT $start_from, $post_per_page";
        $result = $this->db->select($query);
        return $result;
    }

    public function govt_job($start_from, $post_per_page) {
        $query = "SELECT * FROM job_circular WHERE `job_category` = 'Govt Job' ORDER BY id DESC LIMIT $start_from, $post_per_page";
        $result = $this->db->select($query);
        return $result;
    }

    public function bank_job($start_from, $post_per_page) {
        $query = "SELECT * FROM job_circular WHERE `job_category` = 'Bank Job' ORDER BY id DESC LIMIT $start_from, $post_per_page";
        $result = $this->db->select($query);
        return $result;
    }

    public function company_job($start_from, $post_per_page) {
        $query = "SELECT * FROM job_circular WHERE `job_category` = 'Company Job' ORDER BY id DESC LIMIT $start_from, $post_per_page";
        $result = $this->db->select($query);
        return $result;
    }

    public function pharmaceuticals_job($start_from, $post_per_page) {
        $query = "SELECT * FROM job_circular WHERE `job_category` = 'Pharmaceuticals Job' ORDER BY id DESC LIMIT $start_from, $post_per_page";
        $result = $this->db->select($query);
        return $result;
    }

    public function others_job($start_from, $post_per_page) {
        $query = "SELECT * FROM job_circular WHERE `job_category` = 'Others Job' ORDER BY id DESC LIMIT $start_from, $post_per_page";
        $result = $this->db->select($query);
        return $result;
    }

    public function general_knowledge($start_from, $post_per_page) {
        $query = "SELECT * FROM job_circular WHERE `job_category` = 'General Knowledge' ORDER BY id DESC LIMIT $start_from, $post_per_page";
        $result = $this->db->select($query);
        return $result;
    }

    public function pagination() {
        $query = $this->db->select("SELECT * FROM `job_circular`") or die("Error...!!");
        $total_rows = @mysqli_num_rows($query);
        return $total_rows;
    }

    public function pagination_govt_job() {
        $query = "SELECT * FROM `job_circular`  WHERE `job_category` = 'Govt Job'" or die("Error at pagination_govt_job()...!!");
        $result = $this->db->select($query);
        $total_rows = @mysqli_num_rows($result);
        return $total_rows;
    }

    public function pagination_bank_job() {
        $query = "SELECT * FROM `job_circular`  WHERE `job_category` = 'Bank Job'" or die("Error at pagination_bank_job()...!!");
        $result = $this->db->select($query);
        $total_rows = @mysqli_num_rows($result);
        return $total_rows;
    }

    public function pagination_company_job() {
        $query = "SELECT * FROM `job_circular`  WHERE `job_category` = 'Company Job'" or die("Error at pagination_company_job()...!!");
        $result = $this->db->select($query);
        $total_rows = @mysqli_num_rows($result);
        return $total_rows;
    }

    public function pagination_pahrmaceuticals_job() {
        $query = "SELECT * FROM `job_circular`  WHERE `job_category` = 'Pharmaceuticals Job'" or die("Error at pagination_pahrmaceuticals_job()...!!");
        $result = $this->db->select($query);
        $total_rows = @mysqli_num_rows($result);
        return $total_rows;
    } 
    
    public function pagination_others_job() {
        $query = "SELECT * FROM `job_circular`  WHERE `job_category` = 'Others Job'" or die("Error at pagination_others_job()...!!");
        $result = $this->db->select($query);
        $total_rows = @mysqli_num_rows($result);
        return $total_rows;
    }
    
    public function pagination_general_knowledge() {
        $query = "SELECT * FROM `job_circular`  WHERE `job_category` = 'General Knowledge'" or die("Error at pagination_general_knowledge()...!!");
        $result = $this->db->select($query);
        $total_rows = @mysqli_num_rows($result);
        return $total_rows;
    }

    //This function also used for editing posts
    public function jobSinglePost($id) {
        $query = $this->db->select("SELECT * FROM `job_circular` WHERE `id` = '$id'") or die("Error at singlePost()...!!");
        $rows = $query->fetch_assoc();
        return $rows;
    }

    public function jobRelatedPost($category) {
        $_all_catagory = array();
        $query = $this->db->select("SELECT * FROM `job_circular` WHERE `job_category` = '$category' ORDER BY `posted_at` DESC") or die("Error at related_post()!!!");
        while ($rows = $query->fetch_assoc()) {
            $_all_catagory[] = $rows;
        }
        return $_all_catagory;
    }

}
