-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2017 at 08:05 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `personalblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
(1, 'admin', 'admin@yahoo.com', '1');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `likes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `category`, `title`, `body`, `image`, `author`, `tags`, `date`, `likes`) VALUES
(1, 1, 'Create you website using PHP', '\nBack in the early days of Open Source - when Dinosaurs roamed the earth and Rasmus was a young man - there were two types of open source projects we talked about: those that didn''t cost any money, and those that gave you the freedom to redistribute and modify the code. The analogy we used was that projects were "Free as in beer" if they didn''t cost anything, and "Free as in libre" if they gave you the freedom to share with others. That was how we explained Open Source to muggles back then, and there were muggles everywhere. It should be noted that this was also back in the time when the first step in learning PHP was compiling the Linux kernel on your server.', '10405615_337657999727748_8230198474797552045_n_1478959129.jpg', 'Imon', 'PHP Coding', '2016-11-26 05:40:51', 3),
(2, 2, 'Learning JAVA', '<p>\r\nThe continuation of JavaWorld''s most popular long-running series, featuring new and classic Java programming tips from your peers in the Java developer community.\r\n</p>\r\n<p>\r\nThe continuation of JavaWorld''s most popular long-running series, featuring new and classic Java programming tips from your peers in the Java developer community.\r\n</p>\r\n<p>\r\nThe continuation of JavaWorld''s most popular long-running series, featuring new and classic Java programming tips from your peers in the Java developer community.\r\n</p>', '10502401_367577343402480_6151096208276328557_n.jpg', 'Hasnain', 'Java', '2016-11-26 05:40:51', 2),
(3, 3, 'Learn C#', '<p>\r\nI have always rather enjoyed Eric Lippert''s blog "Fabulous Adventures In Coding". It''s not great for staying up to date with the latest technologies but does deal with a lot of the intricacies of C# / .NET. A great site to hone your C# skills.\r\n</p>\r\n<p>\r\nI have always rather enjoyed Eric Lippert''s blog "Fabulous Adventures In Coding". It''s not great for staying up to date with the latest technologies but does deal with a lot of the intricacies of C# / .NET. A great site to hone your C# skills.\r\n</p>\r\n<p>\r\nI have always rather enjoyed Eric Lippert''s blog "Fabulous Adventures In Coding". It''s not great for staying up to date with the latest technologies but does deal with a lot of the intricacies of C# / .NET. A great site to hone your C# skills.\r\n</p>', '13529098_598299386996940_4581055735048435737_n.jpg', 'Ahmed', 'C# learn', '2016-11-26 05:45:17', 4),
(4, 4, 'Learng programming using C++ ', '<p>\r\nThis question exists because it has historical significance, but it is not considered a good, on-topic question for this site, so please do not use it as evidence that you can ask similar questions here. This question and its answers are frozen and cannot be changed. More info: help center.\r\n</p>\r\n<p>\r\nThis question exists because it has historical significance, but it is not considered a good, on-topic question for this site, so please do not use it as evidence that you can ask similar questions here. This question and its answers are frozen and cannot be changed. More info: help center.\r\n</p>\r\n<p>\r\nThis question exists because it has historical significance, but it is not considered a good, on-topic question for this site, so please do not use it as evidence that you can ask similar questions here. This question and its answers are frozen and cannot be changed. More info: help center.\r\n</p>', 'Win10.jpg', 'H A Imon', 'C++', '2016-11-26 05:45:17', 2),
(5, 5, 'HTML Learning', '<p>In HTML 4.01, the tag represents a horizontal rule. However, the tag may still be displayed as a horizontal rule in visual browsers, but is now defined in semantic terms, rather than presentational terms. All the layout attributes are removed in HTML5. Use CSS instead.</p>\r\n<p>In HTML 4.01, the tag represents a horizontal rule. However, the tag may still be displayed as a horizontal rule in visual browsers, but is now defined in semantic terms, rather than presentational terms. All the layout attributes are removed in HTML5. Use CSS instead.</p>', 'images.m (1).jpg', 'Md Ibrahim', 'HTML', '2016-11-26 14:47:01', 2),
(6, 6, 'CSS Learning', '<p>\r\nCascading Style Sheets (CSS) is a simple mechanism for adding style (e.g., fonts, colors, spacing) to Web documents. These pages contain information on how to learn and use CSS and on available software. They also contain news from the CSS working group.\r\n</p>\r\n<p>\r\nCascading Style Sheets (CSS) is a simple mechanism for adding style (e.g., fonts, colors, spacing) to Web documents. These pages contain information on how to learn and use CSS and on available software. They also contain news from the CSS working group.\r\n</p>', '1654324_1005626249458935_4874332089571024500_n1481890239.jpg', 'Md. Riyad', 'CSS', '2016-11-26 14:47:01', 3),
(7, 1, 'Learn PHP Smothly', '<h1>Welcome</h1>\r\n<p>\r\nWelcome to the learn-php.org free interactive PHP tutorial.\r\n\r\nWhether you are an experienced programmer or not, this website is intended for everyone who wishes to learn the PHP programming language.\r\n\r\nThere is no need to download anything - just click on the chapter you wish to begin from, and follow the instructions.\r\n</p>\r\n<p>\r\nGood luck!</p>\r\n', '1654324_1005626249458935_4874332089571024500_n1481890239.jpg', 'Azizul Haque Aziz', 'PHP', '2016-11-28 14:24:55', 2),
(8, 7, 'How can be a good teacher.', 'Set a good example to your students. Remember that you are the teacher. It is important for you to be like a "superhero" figure in their eyes. Remember that your students look up to you and will  try to mimic your disposition. If you are rude or inappropriate, they will have an inappropriate model for their behavior. It is vital that students see you as a person with confidence, so that they follow your lead, and feel comfortable talking to  you. Students, of all ages, need someone they can lean on, look up to, and trust.', '1654324_1005626249458935_4874332089571024500_n1481890239.jpg', 'Imon', 'Teacher', '2016-12-16 12:03:46', 3),
(9, 7, 'How can be a good teacher.', 'Set a good example to your students. Remember that you are the teacher. It is important for you to be like a "superhero" figure in their eyes. Remember that your students look up to you and will  try to mimic your disposition. If you are rude or inappropriate, they will have an inappropriate model for their behavior. It is vital that students see you as a person with confidence, so that they follow your lead, and feel comfortable talking to  you. Students, of all ages, need someone they can lean on, look up to, and trust.', '1654324_1005626249458935_4874332089571024500_n1481890239.jpg', 'Imon', 'Teacher', '2016-12-16 12:05:31', 1),
(10, 7, 'How can be a good teacher.', 'Set a good example to your students. Remember that you are the teacher. It is important for you to be like a "superhero" figure in their eyes. Remember that your students look up to you and will  try to mimic your disposition. If you are rude or inappropriate, they will have an inappropriate model for their behavior. It is vital that students see you as a person with confidence, so that they follow your lead, and feel comfortable talking to  you. Students, of all ages, need someone they can lean on, look up to, and trust.', '1654324_1005626249458935_4874332089571024500_n1481890239.jpg', 'Imon', 'Teacher', '2016-12-16 12:06:48', 7),
(11, 7, 'How can be a good teacher.', 'Set a good example to your students. Remember that you are the teacher. It is important for you to be like a "superhero" figure in their eyes. Remember that your students look up to you and will  try to mimic your disposition. If you are rude or inappropriate, they will have an inappropriate model for their behavior. It is vital that students see you as a person with confidence, so that they follow your lead, and feel comfortable talking to  you. Students, of all ages, need someone they can lean on, look up to, and trust.', '1654324_1005626249458935_4874332089571024500_n1481890239.jpg', 'Imon', 'Teacher', '2016-12-16 12:07:14', 7),
(12, 7, 'How can be a good teacher.', '<p>Set a good example to your students. Remember that you are the teacher. It is important for you to be like a "superhero" figure in their eyes. Remember that your students look up to you and will try to mimic your disposition. If you are rude or inappropriate, they will have an inappropriate model for their behavior. It is vital that students see you as a person with confidence, so that they follow your lead, and feel comfortable talking to you. Students, of all ages, need someone they can lean on, look up to, and trust.</p>', '1654324_1005626249458935_4874332089571024500_n1481890239.jpg', 'Imon', 'Teacher', '2016-12-16 12:10:39', 8);

-- --------------------------------------------------------

--
-- Table structure for table `calculator`
--

CREATE TABLE `calculator` (
  `id` int(11) NOT NULL,
  `addition` int(250) NOT NULL,
  `subtraction` int(250) NOT NULL,
  `multiplication` int(250) NOT NULL,
  `division` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(20) NOT NULL,
  `post_id` int(20) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `comment`, `user_id`, `post_id`, `user_name`, `date`) VALUES
(144, 'hi', 38, 3, 'Azad', '2017-03-24 18:18:55'),
(145, 'hi this is nice post', 38, 12, 'Azad', '2017-03-25 19:31:24'),
(146, 'yes; i agree with you.', 38, 12, 'Azad', '2017-03-25 19:31:47'),
(147, 'yes that is.', 26, 12, 'Ibo', '2017-03-25 20:08:49'),
(148, 'nn', 38, 10, 'Azad', '2017-05-10 17:45:09'),
(149, 'hi', 26, 12, 'Ibo', '2017-05-27 05:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE `form` (
  `id` int(11) NOT NULL,
  `first_name` varchar(80) NOT NULL,
  `last_name` varchar(80) NOT NULL,
  `father_name` varchar(80) NOT NULL,
  `mother_name` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `d_o_birth` date NOT NULL,
  `image` varchar(255) NOT NULL,
  `deleted_at` int(30) DEFAULT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `first_name`, `last_name`, `father_name`, `mother_name`, `email`, `d_o_birth`, `image`, `deleted_at`, `password`) VALUES
(24, 'Hasnain Ahmed', 'Imon', 'Late Md. Wali Ullah', 'Nazma Begum', 'h.a.imon1991@yahoo.com', '2016-10-04', '', NULL, '1'),
(25, 'Azizul Haque', 'Aziz', 'xyz', 'xyz', 'aziz@gmail.com', '2016-10-03', '', NULL, '1'),
(26, 'Md Ibrahim Hossain', 'Ibo', 'xyz', 'xyz', 'ibo123@gmail.com', '2016-10-27', '', NULL, '1'),
(27, 'Md Riyad Hossain', 'Riyad', 'xyz', 'xyzabc', 'riyad@gmail.com', '2016-10-18', '', NULL, '1'),
(29, 'Hasnain Ahmed', 'Imon', 'Late Md. Wali Ullah', 'Nazma Begum', 'imon@yahoo.com', '2016-10-11', '', NULL, '1'),
(30, 'Hasnain Ahmed', 'Imon', 'Late Md. Wali Ullah', 'Nazma Begum', 'imo@gmail.com', '2016-10-04', '', NULL, '1'),
(31, 'Azizul Haque', 'Aziz', 'xyz', 'xyz', 'azaiz123456@gmail.com', '2016-10-10', '', NULL, '1'),
(32, 'Md Kamruzzaman', 'Saikat', 'asdf', 'xyz', 'saikat@gmail.com', '2016-10-18', '', NULL, '1'),
(33, 'Hasnain Ahmed Imon', 'Imo', 'Late Md. Wali Ullah', 'Nazma Begum (House wife)', 'imimon@gmail.com', '2016-11-21', '', NULL, '1'),
(35, 'Asad Khan', 'Hafid', 'Md Father', 'Mrs. Mother', 'asad@gmail.com', '2016-11-22', '10502401_367577343402480_6151096208276328557_n.jpg', NULL, '1'),
(36, 'Ali Mohammad', 'Azam', 'Md Father', 'Mrs Mother', 'azam@gmail.com', '2016-11-29', '13529098_598299386996940_4581055735048435737_n.jpg', NULL, '1'),
(37, 'Md Mohin Khan', 'Mohim', 'Md Ohid Ullah', 'Mrs Mowshumi', 'mohim@gmail.com', '1995-01-01', 'images.m (1).jpg', NULL, '1'),
(38, 'Md Azad', 'Azad', 'xyz', 'adsf', 'azad@yahoo.com', '2016-11-22', 'images.m.image Copy_1479488817.jpg', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `name`) VALUES
(0, 'ghfgh'),
(0, 'fd');

-- --------------------------------------------------------

--
-- Table structure for table `job_circular`
--

CREATE TABLE `job_circular` (
  `id` int(11) NOT NULL,
  `job_category` varchar(250) NOT NULL,
  `official_web` varchar(80) NOT NULL,
  `source` varchar(250) NOT NULL,
  `app_start_date` varchar(12) NOT NULL,
  `app_end_date` varchar(12) NOT NULL,
  `total_post` int(11) NOT NULL,
  `salary` varchar(80) NOT NULL,
  `office_name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(250) NOT NULL,
  `application_link` varchar(500) NOT NULL,
  `job_location` varchar(25) NOT NULL,
  `posted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_circular`
--

INSERT INTO `job_circular` (`id`, `job_category`, `official_web`, `source`, `app_start_date`, `app_end_date`, `total_post`, `salary`, `office_name`, `title`, `image`, `application_link`, `job_location`, `posted_at`) VALUES
(5, 'Bank Job', 'https://www.bb.org.bd/', 'https://erecruitment.bb.org.bd/onlineapp/openpdf.php', '2017-10-03', '2017-10-18', 153, '22,000-53060', '0', 'Bank Circular-2017', '../images/1507742744_bb_job_circular_pic.png', 'https://erecruitment.bb.org.bd/onlineapp/apply_job.php', 'Anywhere in Bangladesh', '2017-10-14 14:39:33'),
(6, 'Govt Job', 'http://www.minland.gov.bd/', 'http://www.minland.gov.bd/', '2017-08-16', '2017-09-08', 30, '8250- 12500', 'à¦­à§‚à¦®à¦¿ à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à§Ÿ', 'à¦­à§‚à¦®à¦¿ à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à§Ÿà§‡ à¦¨à¦¿à§Ÿà§‹à¦— à¦¬à¦¿à¦œà§à¦žà¦ªà§à¦¤à¦¿-à§¨à§¦à§§à§­', '../images/1507920953_ministry-of-land-job-circular.jpg, ../images/1507920953_vumi.jpg', 'à¦¨à¦¾à¦‡', 'Anywhere in Bangladesh', '2017-10-13 19:22:13'),
(7, 'Govt Job', 'http://www.minland.gov.bd/', 'http://www.minland.gov.bd/', '2017-08-16', '2017-09-08', 0, '22,000-53060', 'à¦­à§‚à¦®à¦¿ à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à§Ÿ', 'à¦­à§‚à¦®à¦¿ à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à§Ÿà§‡ à¦¨à¦¿à§Ÿà§‹à¦— à¦¬à¦¿à¦œà§à¦žà¦ªà§à¦¤à¦¿-à§¨à§¦à§§à§­', '../images/1507920953_ministry-of-land-job-circular.jpg, ../images/1507920953_vumi.jpg', 'à¦¨à¦¾à¦‡', 'Anywhere in Bangladesh', '2017-10-13 19:32:58'),
(10, 'Company Job', 'http://www.rangsmotors.com/', 'http://bdjobs.com/', '2017-10-14', '2017-10-20', 10, 'Negotiable', 'Rangs Motors Limited', 'Executive/ Sr. Executive - Admin', '../images/1507991419_rangs.png', 'http://jobs.bdjobs.com/jobdetails.asp?id=728466&ln=3', 'Barisal, Chittagong', '2017-10-14 14:30:19'),
(11, 'Bank Job', 'https://www.bb.org.bd/', 'https://erecruitment.bb.org.bd/onlineapp/openpdf.php', '2017-10-14', '2017-10-18', 153, '22,000-53060', 'Bangladesh Bank', 'Shonali-Janata Bank job Circular', '../images/1507992174_1507742744_bb_job_circular_pic.png.png', 'https://erecruitment.bb.org.bd/onlineapp/apply_job.php', 'Anywhere in Bangladesh', '2017-10-14 14:43:16');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `post_id`) VALUES
(24, 35, 10),
(25, 35, 12),
(26, 35, 11),
(30, 38, 11),
(31, 38, 12),
(32, 38, 10),
(34, 38, 8),
(35, 33, 10),
(42, 33, 5),
(44, 33, 11),
(46, 33, 12),
(47, 27, 7),
(48, 27, 4),
(49, 27, 5),
(50, 27, 6),
(52, 27, 11),
(53, 27, 10),
(54, 27, 9),
(55, 27, 8),
(56, 27, 3),
(57, 27, 2),
(59, 27, 1),
(61, 27, 12),
(63, 26, 10),
(64, 32, 8),
(65, 32, 7),
(66, 32, 3),
(67, 32, 4),
(68, 32, 6),
(69, 32, 2),
(73, 24, 12),
(74, 30, 1),
(76, 30, 11),
(77, 30, 10),
(80, 30, 12),
(81, 26, 3),
(82, 26, 11),
(83, 26, 6),
(84, 38, 3),
(85, 38, 1),
(86, 26, 12),
(87, 24, 11),
(88, 24, 10),
(89, 37, 12);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `name`) VALUES
(1, 'PHP'),
(2, 'JAVA'),
(3, 'C#'),
(4, 'C++'),
(5, 'HTML'),
(6, 'CSS'),
(7, 'Teacher'),
(8, 'Bank Job'),
(9, 'Govt Job'),
(10, 'Company Job'),
(11, 'Pharmaceuticals Job'),
(12, 'Others Job'),
(13, 'Blog'),
(14, 'General Knowledge');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calculator`
--
ALTER TABLE `calculator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `job_circular`
--
ALTER TABLE `job_circular`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `calculator`
--
ALTER TABLE `calculator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;
--
-- AUTO_INCREMENT for table `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `job_circular`
--
ALTER TABLE `job_circular`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
