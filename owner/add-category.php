<?php
include 'header.php';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    if (empty($name)) {
        echo "<script>alert('Field can not be empty!!!')</script>";
    } else {
        $admin->category_store($name);
        echo "<script>alert('New category added successfully.')</script>";
    }
}
?>
<div class="row">
    <?php include 'sidebar.php'; ?>
    <div class="col-10">
        <div class=" row justify-content-md-center" style="padding-top: 5%;">
            <div class="col-4">
                <form action="" method="post">
                    <label class="control-label">Category</label>
                    <input class="form-control" type="text" name="name">
                    <button class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>