<?php include 'header.php';?>
<div class="row">
    <?php include 'sidebar.php'; ?>
    <div class="col-9">
        <hr>
        
        <form method="get" action="job-circular" style="display: inline">
            <button class="btn btn-lg btn-primary">Job Circular</button>
        </form>
        <form method="get" action="blog-post" style="display: inline">
            <button class="btn btn-lg btn-primary">Blog Post</button>
        </form>
    </div>
    <div class="col-1"></div>
</div>
<?php include 'footer.php';?>