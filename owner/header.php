<?php
session_start();
use App\controller\controller_class\Admin;
use App\controller\controller_class\details;
use App\controller\controller_class\blog;
use App\controller\controller_class\helper;
use App\controller\controller_class\Circular;

include_once ($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "personalblog" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");


$admin      = new Admin();
$users      = new details();
$helper     = new helper();
$blog       = new blog();
$circular   = new Circular();

$all_post   = $blog->index();
if (!$admin->getSession()) {
    header("location: login.php");
}
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['store_circular'])) {
    $circular->store($_FILES, $_POST);
}
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['logout'])) {
    $admin->logout();
    header("location: login.php");
}


 ?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Admin Panel</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../design/DataTables-1.10.16/css/jquery.dataTables.min.css"/>
        <link href="../design/css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../design/bootstrap-4.0.0-beta/dist/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../design/bootstrap-4.0.0-beta/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>
        body{}
        h1, h2, h3, h4, h5, h6, p {
            padding: 0;
            margin: 0;
        }
    </style>
    <body>


        <!--Header-->
        <div class="adminheader" style="font-size: 20px;">
            <a style="">Welcome <?= ucfirst($_SESSION['admin_name'])?></a> | <a style="color: #FFF" href="logout.php">Logout</a>
        </div>
        <!--End Header-->