<?php include 'header.php'; ?>
<div class="row">
    <?php include 'sidebar.php'; ?>
    <div class="col-9">
        <div class="">
            <a href="add-post" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-plus"> Add New Post</span></a>
        </div>
        <hr>
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Sl No</th>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Start date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Sl No</th>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Start date</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                $i = 0;
                foreach ($all_post as $posts) {
                    $i++;
                    ?>
                    <tr>
                        <td><?= $i; ?></td>
                        <td><?= $posts['author'] ?></td>
                        <td><?= $posts['title'] ?></td>
                        <td><?= $posts['name'] ?></td>
                        <td><?= $helper->dateFormat($posts['date']); ?></td>
                        <td>
                            <a href="view-post?id=<?= $posts['id'] ?>" class="glyphicon glyphicon-eye-open"></a>
                            <a href="edit-post?id=<?= $posts['id'] ?>" class="glyphicon glyphicon-edit"></a>
                            <a href="delete-post?id=<?= $posts['id'] ?>" class="glyphicon glyphicon-trash"></a>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>

        <?php
        $all_circular = $circular->select();
        if ($all_circular) {
            ?>
            <hr>
            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Title</th>
                        <th>Office Name</th>
                        <th>Category</th>
                        <th>Total Post</th>
                        <th>Location</th>
                        <th>image</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Sl No</th>
                        <th>Title</th>
                        <th>Office Name</th>
                        <th>Category</th>
                        <th>Total Post</th>
                        <th>Location</th>
                        <th>image</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $i = 0;
                    while ($job_circular = $all_circular->fetch_assoc()) {
                        $i++;
                        ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?= $job_circular['title'] ?></td>
                            <td><?= $job_circular['office_name'] ?></td>
                            <td><?= $job_circular['job_category'] ?></td>
                            <td><?= $job_circular['total_post'] ?></td>
                            <td><?= $job_circular['job_location'] ?></td>
                            <td><img src="<?= $job_circular['image'] ?>" width="50" height="50"></td>
                            <td><?= $helper->dateFormatSmall($job_circular['app_start_date']); ?></td>
                            <td><?= $helper->dateFormatSmall($job_circular['app_end_date']); ?></td>
                            <td>
                                <a href="view-post?id=<?= $job_circular['id'] ?>" class="glyphicon glyphicon-eye-open"></a>
                                <a href="edit-post?id=<?= $job_circular['id'] ?>" class="glyphicon glyphicon-edit"></a>
                                <a href="delete-post?id=<?= $job_circular['id'] ?>" class="glyphicon glyphicon-trash"></a>
                            </td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        <?php } ?>
    </div>
    <div class="col-1">

    </div>
</div>
<?php include 'footer.php'; ?>