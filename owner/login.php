<?php
session_start();
use App\controller\controller_class\Admin;
include_once ($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "personalblog" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");

$admin      = new Admin();


if ($admin->getSession()) {
    header("location: index.php");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])) {
    $name = $_POST['name'];
    $pass = $_POST['password'];
    $admin->login($name, $pass);
        header("location: index.php");
}

 ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Admin Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../design/bootstrap-4.0.0-beta/dist/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../design/bootstrap-4.0.0-beta/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body>
        <div class="container" style="padding-top: 5%;">
            <div class="row justify-content-sm-center">
                <div class="col col-sm-4 offset-sm-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label class="control-label" for="name">User Name</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="name">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Password</label>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                </div>
                                <button type="submit" name="login" class="btn btn-primary btn-block">Sign In</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../design/js/jquery.min.js"></script>
        <script src="../design/bootstrap-4.0.0-beta/dist/js/bootstrap.min.js"></script>
        <script src="../design/bootstrap-4.0.0-beta/dist/js/bootstrap.js"></script>
    </body>
</html>
