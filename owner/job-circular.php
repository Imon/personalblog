<?php include 'header.php'; ?>
<div class="row">
    <?php include 'sidebar.php'; ?>
    <div class="col-10">
        <form action="" method="post" style="padding-top: 2%;" enctype="multipart/form-data">
            <div class="container row">
                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="title" class="col-sm-3 col-form-label">Title</label>
                        <div class="col-sm-9">
                            <input type="text" name="title" class="form-control" id="title" placeholder="Title">
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="office_name" class="col-sm-3 col-form-label">Office Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="office_name" class="form-control" id="office_name" placeholder="Office Name">
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="source" class="col-sm-3 col-form-label">Source</label>
                        <div class="col-sm-9">
                            <input type="text" name="source" class="form-control" id="source" placeholder="Source">
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="official_web" class="col-sm-3 col-form-label">Website</label>
                        <div class="col-sm-9">
                            <input type="text" name="official_web" class="form-control" id="official_web" placeholder="Website">
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="job_category" class="col-sm-3 col-form-label">Job Category</label>
                        <div class="col-sm-9 ">
                            <select name="job_category" id="job_category" class="">
                                <option value="">--Please Select--</option>
                                <?php
                                $all_category = $blog->category();
                                foreach ($all_category as $category) {
                                    ?>
                                    <option value="<?= $category['name'] ?>"><?= $category['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="total_post" class="col-sm-3 col-form-label">Total Post</label>
                        <div class="col-sm-9">
                            <input type="number" name="total_post" class="form-control" min="1" id="total_post">
                        </div>
                    </div>
                </div>



                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="app_start_date" class="col-sm-3 col-form-label">Start Date</label>
                        <div class="col-sm-9">
                            <input type="date" name="app_start_date" class="form-control" id="app_start_date" placeholder="dd/mm/year">
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="app_end_date" class="col-sm-3 col-form-label">End Date</label>
                        <div class="col-sm-9">
                            <input type="date" name="app_end_date" class="form-control" id="app_end_date" placeholder="dd/mm/year">
                        </div>
                    </div>
                </div>



                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="salary" class="col-sm-3 col-form-label">Salary</label>
                        <div class="col-sm-9">
                            <input type="text" name="salary" class="form-control" id="salary" placeholder="Salary">
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="application_link" class="col-sm-3 col-form-label">Application Link</label>
                        <div class="col-sm-9">
                            <input type="text" name="application_link" class="form-control" id="application_link" placeholder="Application Link">
                        </div>
                    </div>
                </div>


                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="job_location" class="col-sm-3 col-form-label">Job Location</label>
                        <div class="col-sm-9">
                            <input type="text" name="job_location" class="form-control" id="job_location" placeholder="Job Location">
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="image" class="col-sm-3 col-form-label">Circular Image</label>
                        <div class="col-sm-9">
                            <input type="file" name="file[]" class="form-control" id="image" multiple accept="image/*">
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <button class="btn btn-primary" name="store_circular">Store Circular</button>
                </div>
            </div>
        </form>
    </div>
    <?php include 'footer.php'; ?>