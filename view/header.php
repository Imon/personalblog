<?php
session_start();

use App\controller\controller_class\Admin;
use App\controller\controller_class\details;
use App\controller\controller_class\blog;
use App\controller\controller_class\helper;
use App\controller\controller_class\Circular;

include_once ($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "personalblog" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");
$users = new details();
$helper = new helper();
$blog = new blog();
$circular = new Circular();


$blog_list = $blog->category();
$recent_post = $blog->select_for_sidebar();
/*
  if ($users->getSession()) {
  header("location: views/all_files/details/show.php");
  }
 */
?>


<!DOCTYPE html>
<html>
    <head>
        <title>Imon | My Personal Blog | Home </title>
        <link href="../design/css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../design/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href='../design/fonts/fonts.css' rel='stylesheet' type='text/css'>
        <link href='../design/fonts/fonts1.css' rel='stylesheet' type='text/css'>
        <link href='../design/fonts/fonts2.css' rel='stylesheet' type='text/css'>
        <!-- js -->
        <script src="../design/js/jquery.min.js"></script>
        <!-- //js -->
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Floral Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //for-mobile-apps -->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="../design/js/move-top.js"></script>
        <script type="text/javascript" src="../design/js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        <!-- start-smoth-scrolling -->
    </head>

    <body style="background-color: gray">
        <!-- header -->
        <div class="fixed-header">
            <div class="col-xs-12">
                <div class="">
                    <div class="row">
                        <div class="top-header">
                            <ul class="nav1">
                                <?php if ($users->getSession()) { ?>
                                    <li class="cap hovers"><a href=""><?= "Hello " . $_SESSION['user_name']; ?></a></li>
                                    <li class="cap hovers"><a href="logout">Logout</a></li>
                                <?php } else { ?>
                                    <form action="" method="post" class="form-inline" style="color: #fff; display: inline; font-style: italic; font-size: 13px; text-align: left;">
                                        <label class="control-label" for="search">Search</label>
                                        <input style="height: 27px; background-color: #1B242F; color: #fff;" type="search" name="search" class="form-control mb-2 mr-sm-2 mb-sm-0" id="search" placeholder="Search....">
                                    </form>
                                    <form action="" method="post" class="form-inline" style="color: #fff; display: inline; font-style: italic; font-size: 13px;">
                                        <label class="control-label" for="userName">User Name</label>
                                        <input style="height: 27px; background-color: #1B242F; color: #fff;" type="text" name="name" class="form-control mb-2 mr-sm-2 mb-sm-0" id="userName" placeholder="User Name">
                                        
                                        <label class="control-label" for="Password">Password</label>
                                        <input style="height: 27px; background-color: #1B242F; color: #fff;" type="password" name="password" class="form-control mb-2 mr-sm-2 mb-sm-0" id="Password" placeholder="*****">
                                        <button type="submit" class="btn btn-primary btn-sm">Login</button>
                                    </form>
                                    <a style="color: #fff; text-decoration: none;" href="register"><button type="submit" class="btn btn-primary btn-sm" name="register" style="display: inline;">Register</button></a>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>            
            </div>
            <div class="header">
                <div class="container">
                    <div class="header-info">
                        <div class="logo">
                            <a href="index" style="text-decoration: none;">
                            <h1 style='color: #002752; font-style: italic; font-weight: bold; cursor: pointer;'>Imon's Blog</h1>
                            <p style="color: #004085; font-style: italic; font-weight: bold; cursor: pointer;">For your help</p>
                            </a>
                        </div>
                        <div class="logo-right">
                            <span class="menu"><img src="images/menu.png" alt=" "/></span>
                            <ul class="nav1">
                                <li class="cap"><a href="index">Home</a></li>
                                <li><a href="govt-job">Govt Job</a></li>
                                <li><a href="company-job">Company Job</a></li>
                                <li><a href="bank-job">Bank Job</a></li>
                                <li><a href="pharmaceuticals-job">Pharmaceuticals Job</a></li>
                                <li><a href="others-job">Others Job</a></li>
                                <li><a href="all-blog">Blog</a></li>
                                <li><a href="general-knowledge">General Knowledge</a></li>
                                <li><a href="contact">Contact</a></li>
                            </ul>
                        </div>
                        <div class="clearfix"> </div>
                        <!-- script for menu -->
                        <script>
                            $("span.menu").click(function () {
                                $("ul.nav1").slideToggle(300, function () {
                                    // Animation complete.
                                });
                            });
                        </script>
                        <!-- //script for menu -->
                    </div>
                </div>
            </div>
        </div>
        <!-- header -->