<?php
include 'header.php';
?>
<!-- content -->
<div class="content">
    <div class="container">	
        <div class="content-text">
            <div class="title">
                <?php
                //pagination
                $post_per_page = 6;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 1;
                }
                $start_from = ($page - 1) * $post_per_page;



                $pagignation = $blog->pagination();
                $total_page = ceil($pagignation / $post_per_page);
                for ($i = 1; $i <= $total_page; $i++) {
                    echo "<span class='pagination'><a href='all-blog?page=$i'>" . $i . " |</a></span>";
                }echo '<span><br></span>';
                //Pagignation end
                
                $blog_post = $blog->select($start_from, $post_per_page);
                if(!empty($blog_post)){
                foreach ($blog_post as $new_post) {
                    ?>
                    <div class="" style="width: 45%; display: inline-block; margin-right: 4%; margin-bottom: 5%;">
                        <div class="some-title">
                            <h4><a href="single_post?id=<?= $new_post['id'] ?>"><?= $new_post['title']; ?></a></h4><br>
                        </div>
                        <br><br>
                        <hr>
                        <div class="john">
                            <p>Posted By : <a href="#"><?= $new_post['author']; ?></a><span><?= "Date  : " . $helper->dateFormat($new_post['date']); ?></span></p>
                        </div>
                        <div class="clearfix"> </div>
                        <div class="tilte-grid">
                            <a href="single_post?id=<?= $new_post['id'] ?>"><img src="../images/<?= $new_post['image']; ?>" alt=" " /></a>
                            <p class="Sed">
                                <span><?= $helper->textShorten($new_post['body']); ?>
                                    <a class="read-more" href="single_post?id=<?= $new_post['id'] ?>">Read More</a></span> 
                            </p> 
                        </div>
                    </div>
                <?php } }else{?> <h1 class="text-info"><b><?php echo "No Records Found";}?> </b></h1>

            </div>
            <?php include './sidebar.php' ?>
            <div class="clearfix"> </div>
            <?php
            for ($i = 1; $i <= $total_page; $i++) {
                echo "<span class='pagination'><a href='index?page=$i'>" . $i . " |</a></span>";
            }echo '<span><br></span>';
            ?>
        </div>

    </div>
</div>
<!-- //content -->
<?php include 'footer.php'; ?>