<!-- footer -->
        <div class="footer">
            <div class="container">
                
                <div class="footer-grids">
                    <div class="footer-grid">
                        <h3>About Us</h3>
                        <p>Nullam ac urna velit. Pellentesque in arcu tortor. 
                            Pellentesque nec est et elit varius pulvinar eget vitae sapien. 
                            Aenean vehicula accumsan gravida. Cum sociis natoque penatibus
                            et magnis dis parturient montes, nascetur ridiculus mus. Phasellus 
                            et lectus in urna consequat consectetur ut eget risus. Nunc augue diam, 
                            mattis eu tristique luctus, aliquam vitae massa. Praesent lacinia nisi 
                            sit amet risus cursus porta.</p>
                    </div>
                    <div class="footer-grid">
                        <h3>Site Page</h3>
                        <ul>
                            <li class="cap1"><a href="index">Home</a></li>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Gallery</a></li>
                            <li><a href="contact">Contact</a></li>
                        </ul>
                    </div>
                    <div class="footer-grid">
                        <h3>Praesent pharetra</h3>
                        <ul>
                            <li><a href="single.html">Vestibulum iaculis scelerisque</a></li>
                            <li><a href="single.html">Cras aliquam erat</a></li>
                            <li><a href="single.html">Morbi imperdiet ipsum</a></li>
                            <li><a href="single.html">Donec faucibus mollis</a></li>
                            <li><a href="single.html">Praesent lacinia nisi</a></li>
                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <p style="text-align: center">
                    Copyright &copy;<a href="https://www.facebook.com/hasnain.imon"> Imon</a> | 
                    Follow Us on :
                    <a href="https://www.facebook.com/hasnain.imon" target="_blank"><span class="fa fa-facebook-official fa-2x" style="background-color: blue;"></span></a>
                    <a href="" ><span class="fa fa-google-plus-circle fa-2x" style="background-color: #d34836;"  target="_blank"></span></a>
                    <a href="https://www.linkedin.com/in/hasnain-ahmed-imon-077085b7/"><span target="_blank" class="fa fa-2x fa-linkedin-square" style="background-color: #0077B5;"></span></a>
                    <a href="https://twitter.com/Ims_im0n"><span class="fa fa-twitter-square fa-2x" style="background-color: #0084b4; " target="_blank"></span></a>
                
                </p>
            </div>
        </div>
        <!-- //footer -->
        <!-- here stars scrolling icon -->
        <script type="text/javascript">
            $(document).ready(function () {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear' 
                 };
                 */

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <script src="https://use.fontawesome.com/4708977b9b.js"></script>
        <!-- //here ends scrolling icon -->
    </body>
</html>