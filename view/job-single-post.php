<?php
include './header.php';
if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['id'])) {
    $id = $_GET['id'];
    $single_post = $circular->jobSinglePost($id);
    $category_id = $single_post['job_category'];
    $related_post = $circular->jobRelatedPost($category_id);
} else {
    header("location: index");
}
?>

<!-- content -->
<div class="content">
    <div class="container">	
        <div class="content-text">
            <div class="title">
                <div class="some-title">
                    <h4><?= $single_post['title']; ?></h4><br>
                    <div class="john">
                        <p style="text-align: left;">Posted Date : <span><?= $helper->dateFormat($single_post['posted_at']); ?></span></p>
                    </div>
                </div>
                <!--                <div class="john">-->
                <!--                </div>-->
                <div class="clearfix"> </div>
                <div class="tilte-grid">
                    <p class="Sed">
                        নিয়োগকারী কর্তৃপক্ষের নামঃ <?= $single_post['office_name']; ?> 
                    </p>
                    <p class="Sed">
                        আবেদন শুরুর তারিখঃ <?= $helper->dateFormat($single_post['app_start_date']); ?> 
                    </p> 

                    <p class="Sed">
                        আবেদন জমাদানের শেষ তারিখঃ <?= $helper->dateFormat($single_post['app_end_date']); ?> 
                    </p> 
                    <p class="Sed">
                        পদের সংখ্যাঃ <?= $single_post['total_post']; ?> 
                    </p> 

                    <p class="Sed">
                        বেতনঃ <?= $single_post['salary']; ?> 
                    </p> 

                    <p class="Sed">
                        জব লোকেশনঃ <?= $single_post['job_location']; ?> 
                    </p> 

                    <p class="Sed">
                        অনলাইনে আবেদনের লিংকঃ <a href="<?= $single_post['application_link']; ?> " target="_blank"><b>এখানে ক্লিক করুন</b></a> 
                    </p>

                    <?php if (!empty($single_post['image'])): ?>

                        <?php
                        $array = explode(',', $single_post['image']);
                        foreach ($array as $value) {
                            $value = str_replace(' ', '', $value);
                            ?>
                            <img alt="" src="<?= $value ?>" height="1000" width="730"/>
                        <?php } ?>
                        <!--<img src="<?//= $single_post['image']; ?>" height="1000" width="730" alt=" " />-->

                    <?php endif;
                    ?>
                </div>
                <hr>
                <h3>Related Post</h3>
                <?php
                foreach ($related_post as $all_related_post) {
                    ?>
                    <div class="" style="width: 28%; display: inline-block; margin-right: 4%; margin-bottom: 2%; margin-top: 5%; border: 1px solid #000; text-align: center; ">
                        <h6 style=""><a href="job-single-post?id=<?= $all_related_post['id'] ?>"><?= $all_related_post['title']; ?></a></h6><br>

                        <?php if (!empty($all_related_post['image'])): ?>

                            <?php
                            $array = explode(',', $all_related_post['image']);
                            foreach ($array as $value) {
                                $value = str_replace(' ', '', $value);
                                ?>
                                <a href="job-single-post?id=<?= $all_related_post['id'] ?>"><img alt="" src="<?= $value ?>" height="100" width="100"/></a>
                            <?php } ?>
                            <!--<a href="job-single-post?id=<?//= $all_related_post['id'] ?>"><img style="margin-bottom: 10%" src="<?//= $all_related_post['image']; ?>" alt=" " height="100" width="150" /></a>-->

                        <?php endif;
                        ?>



                    </div>
                <?php } ?>
            </div>
            <?php include './sidebar.php' ?>
            <div class="clearfix"> </div>
        </div>

    </div>
</div>
<?php
include './footer.php';
?>
