<?php
include './header.php';
?>
<!-- content -->
<div class="content">
    <div class="container">	
        <div class="content-text">
            <div class="title">
                <h5 style="color: red">* Required Field</h5> <br><br>
                <form>
                    <div class="form-group">
                        <label for="name">Name</label><span style="color: red">*</span>
                        <input type="text" class="form-control" name="name" id="name" required placeholder="Your Name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label><span style="color: red">*</span>
                        <input type="text" name="email" class="form-control" required id="email" placeholder="email@gmail.com">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="number">Mobile Number</label><span style="color: red">*</span>
                        <input type="number" name="number" class="form-control" required id="number" placeholder="12345">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your number with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="message">Your Message</label><span style="color: red">*</span>
                        <textarea class="form-control" name="message" id="message" cols="10" rows="10" style = "resize: none" required></textarea>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="" name="checkbox" id="checkbox" value="Newsletter"> <label for="checkbox">Send me newsletters also</label>
                    </div>
                    <br><br><br>
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="button" aria-pressed="false" autocomplete="off">Send Your Message</button>
                </form>
            </div>
            <?php include './sidebar.php' ?>
            <div class="clearfix"> </div>

        </div>

    </div>
</div>
<!-- //content -->
<?php include 'footer.php'; ?>