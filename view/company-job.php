<?php
include 'header.php';
$pagignation = $circular->pagination_company_job();
if(empty($pagignation)){
    echo "<script>alert('There is no category available for company circular!!!')</script>";  
}
?>
<!-- content -->
<div class="content">
    <div class="container">	
        <div class="content-text">
            <div class="title">
                <?php
                //pagination
                $post_per_page = 6;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 1;
                }
                $start_from = ($page - 1) * $post_per_page;
               // $pagignation = $circular->pagination_company_job();
                $total_page = ceil($pagignation / $post_per_page);
                for ($i = 1; $i <= $total_page; $i++) {
                    echo "<span class='pagination'><a href='company-job?page=$i'>" . $i . " |</a></span>";
                }echo '<span><br></span>';
                //Pagignation end
                $all_circular = $circular->company_job($start_from, $post_per_page);
                if (!empty($all_circular)) {
                    while ($job_circular = $all_circular->fetch_assoc()) {
                        ?>


                        <div class="" style="width: 45%; display: inline-block; margin-right: 4%; margin-bottom: 5%;">
                            <div class="some-title">
                                <h4><a href="job-single-post?id=<?= $job_circular['id'] ?>"><?= $job_circular['title']; ?></a></h4><br>
                            </div>
                            <br><br>
                            <hr>
                            <div class="john">
                                <p><a href="#"></a><span><?= "Start Date  : " . $helper->dateFormat($job_circular['app_start_date']); ?></span></p>
                                <p><a href="#"></a><span><?= "End Date  : " . $helper->dateFormat($job_circular['app_end_date']); ?></span></p>
                            </div>
                            <div class="clearfix"> </div>
                            <?php if (!empty($job_circular['image'])): ?>
                                <div class="tilte-grid">
                                    <?php
                                    $array = explode(',', $job_circular['image']);
                                    foreach ($array as $value) {
                                        $value = str_replace(' ', '', $value);
                                        ?>
                                        <a rel="example_group" href="job-single-post?id=<?= $job_circular['id']; ?>" title=""><img alt="" src="<?= $value ?>" height="150"/></a>
                                    <?php }
                                    ?>
                                    <!--<a href="job-single-post?id=<?//= $job_circular['id'] ?>"><img src="<?//= $job_circular['image']; ?>" height="150" alt=" " /></a>-->
                                <?php endif; ?>
                                <p class="Sed">
                                    Total Post :  <?= $job_circular['total_post']; ?>
                                </p> 
                            </div>
                        </div>


                    <?php }
                } else {  ?>  <h1 class="text-info"><b><?php echo "No Job Posted Yet";}?> </b></h1>

            </div>
            <?php include './sidebar.php' ?>
            <div class="clearfix"> </div>
            <?php
            for ($i = 1; $i <= $total_page; $i++) {
                echo "<span class='pagination'><a href='index?page=$i'>" . $i . " |</a></span>";
            }echo '<span><br></span>';
            ?>
        </div>

    </div>
</div>
<!-- //content -->
<?php include 'footer.php'; ?>