<?php
include './header.php';
if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['id'])) {
    $id = $_GET['id'];
    $single_post = $blog->singlePost($id);
    $category_id = $single_post['category'];
    $related_post = $blog->related_post($category_id);
} else {
    header("location: index");
}
?>

<!-- content -->
<div class="content">
    <div class="container">	
        <div class="content-text">
            <div class="title">

                <div class="some-title">
                    <h4><?= $single_post['title']; ?></h4><br>
                    <div class="john">
                        <p>Posted By : <a href="#"><?= $single_post['author']; ?></a><span><?= "Date  : " . $helper->dateFormat($single_post['date']); ?></span></p>
                    </div>
                </div>
                <br><hr>
                <div class="clearfix"> </div>
                <div class="tilte-grid">
                    <p class="Sed">
                        <span><?= $single_post['body']; ?> 
                    </p> 
                    <img src="../images/<?= $single_post['image']; ?>" height="1000" width="730" alt=" " />
                </div>
                <hr>
                <h3>Related Post</h3>
                <?php
                foreach ($related_post as $all_related_post) {
                    ?>
                    <div class="" style="width: 28%; display: inline-block; margin-right: 4%; margin-bottom: 2%; margin-top: 5%; border: 1px solid #000; text-align: center; ">
                        <h6 style=""><a href="single_post?id=<?= $all_related_post['id'] ?>"><?= $all_related_post['title']; ?></a></h6><br>
                        <a href="single_post?id=<?= $all_related_post['id'] ?>"><img style="margin-bottom: 10%" src="../images/<?= $all_related_post['image']; ?>" alt=" " height="100" width="150" /></a>
                    </div>
                <?php } ?>
            </div>
            <?php include './sidebar.php' ?>
            <div class="clearfix"> </div>
        </div>

    </div>
</div>
<?php
include './footer.php';
?>

