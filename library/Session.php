<?php
/*
* Session class
*/

class Session{

	// Session Initialization
	public static function init(){

		if(version_compare( phpversion(), '5.4.0', '<' )){
			if(empty(session_id())) session_start();
		}else{
			if(session_status() == PHP_SESSION_NONE) session_start();
		}
	}
	// Session set
	public static function session_set($key, $val){
		$_SESSION[$key] = $val;
	}

	// Session get
	public static function session_get($key){
		if(isset($_SESSION[$key])){
			return $_SESSION[$key];
		}else{
			return false;
		}
	}

	// Session check
	public static function session_check(){
		self::init();
		if(self::session_get('login_status') == FALSE){
			self::session_destroy();
		}
	}

	// Session check login state
	public static function check_login(){
		self::init();
		if(self::session_get('login_status') == TRUE){
			header("Location:dashboard.php");
		}
	}

	// Session session destroy
	public static function session_destroy(){
		session_destroy();
		header("Location:../admin/index.php");
	}

	// Session session destroy
	public static function user_destroy(){
		session_destroy();
		header("Location:../login.php");
	}
}

?>